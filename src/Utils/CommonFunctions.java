package Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class CommonFunctions {
	
public String ReadFromPropertyfile(String property) throws IOException
{
	
	File file = new File (System.getProperty("user.dir")+"\\src\\Utils\\EnvironmentDetails.properties");
	//("C:\\Users\\chaya.ps\\workspace\\TestProject\\src\\Utils\\EnvironmentDetails.properties");
	FileInputStream fileInput = null;
	
	fileInput = new FileInputStream(file);

	Properties prop = new Properties();
	prop.load(fileInput);
	
	return prop.getProperty(property);
	 
}
}

