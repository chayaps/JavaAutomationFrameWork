package TestCases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Controls.Logincontrols;
import Reporting.Log4j;
import Utils.CommonFunctions;
import Utils.Constant;
import Utils.ExcelUtils;


public class NewTest {

	public ChromeDriver driver;
	public Logincontrols loginControls;
	public Utils.CommonFunctions commonFunction;
	public Reporting.Log4j log;
	
	
	NewTest()
  {
	  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "\\ExternalLibraries\\chromedriver.exe");
	  commonFunction=new CommonFunctions();
	  driver = new ChromeDriver (); 
	  loginControls=new Logincontrols();
	  
	  
  }
  
  @BeforeTest
  public void init() throws IOException
  {
	  driver.get(commonFunction.ReadFromPropertyfile("OM_TestServer_URL"));
	  driver.manage().timeouts().implicitlyWait(9000, TimeUnit.SECONDS);
	  
  }
	
  @Test
  public void Test() throws Exception {
	  try {
	  
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		
		Log4j.startTestCase("test");
		Log4j.info("Trial");
		driver.findElementById(Logincontrols.userName).sendKeys(commonFunction.ReadFromPropertyfile("OM_TestServer_User"));
		driver.findElementById(Logincontrols.password).sendKeys(commonFunction.ReadFromPropertyfile("OM_TestServer_Password"));
		driver.findElementByXPath(Logincontrols.loginButton).click();
		ExcelUtils.setCellData("Pass", 1, 3);
		
		Log4j.endTestCase("test");
	} 
	  catch (Exception e) {
		
		e.printStackTrace();
		ExcelUtils.setCellData("Fail", 1, 3);
	}
  }
  
  @Test
  public void Test1() throws Exception {
	  try {
	  
		
		
		Log4j.startTestCase("test");
		Log4j.info("Trial");
		driver.findElementById(Logincontrols.userName).sendKeys(commonFunction.ReadFromPropertyfile("OM_TestServer_User"));
		driver.findElementById(Logincontrols.password).sendKeys(commonFunction.ReadFromPropertyfile("OM_TestServer_Password"));
		driver.findElementByXPath(Logincontrols.loginButton).click();
		
		
		Log4j.endTestCase("test");
	} 
	  catch (Exception e) {
		
		e.printStackTrace();
		ExcelUtils.setCellData("Fail", 1, 3);
	}
  }
  
  @AfterTest
  public void CloseDriver()
  {
	  driver.close();
  }

}
